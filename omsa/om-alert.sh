#!/bin/bash

#----------------------------------#
#    Dell OpenMange Email Alert    #
#----------------------------------#

SERVERNAME="peltz-backup.stanford.edu"
ALERT_MSG_EMAILS="akeem@stanford.edu,kelvinl@stanford.edu,gman@stanford.edu,ruh@stanford.edu"

echo "There has been an OpenManage [$1] detected on $SERVERNAME.  Please login to the web interface to see details. https://$SERVERNAME:1311" | mail -s "$SERVERNAME Server Alert!" akeem@stanford.edu # $ALERT_MSG_EMAILS
