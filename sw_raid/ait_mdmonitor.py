#!/usr/bin/env python3 

import socket
import subprocess
import datetime
import smtplib
import email

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

nodename = socket.getfqdn()
raid_device = '/dev/md126'
raid_out = subprocess.check_output(['/sbin/mdadm', '--detail', raid_device], encoding='utf-8')

mail_server  = smtplib.SMTP('smtp-unencrypted.stanford.edu', 25)
mail_sender = 'MDRAID-ADMIN@' + nodename.upper()
mail_recipient = 'anes-sysadmin@lists.stanford.edu'
mail_subject = '[anes-sysadmin] CRITICAL | MDRAID Event occured on: ' + nodename.upper()

def send_alert():
	# create message object
	msg = MIMEMultipart()
	
	# format message
	msg['From']=mail_sender
	msg['To']='akeem@stanford.edu'
	msg['Subject']=mail_subject
	
	# attach output to message body
	msg.attach(MIMEText(raid_out, 'plain'))
	
	# send message out
	mail_server.send_message(msg)

	# close smtp connection
	mail_server.quit()

def main():
	# find the current state of the mirror
	for line in raid_out.split('\n'):
		if 'State :' in line:
			raid_state = line.split()[2]
	
	current_time = datetime.datetime.now()
	disk_check_end = current_time.replace(hour=2, minute=30, second=0, microsecond=0)
	
	# check if raid if the state healthy
	if raid_state == 'active' or raid_state == 'clean':
		print('state looks good: ' + raid_state)
		pass
	elif raid_state == 'active,' or raid_state == 'clean,':
		if current_time > disk_check_end: # integrity checks should finish by 2:30AM
			print('sending alert')
			send_alert()
	else:
		print('sending alert')
		send_alert()
	
if __name__ == '__main__':
    main()